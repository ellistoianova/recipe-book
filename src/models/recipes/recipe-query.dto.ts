import { IsString } from 'class-validator';

export class RecipeQueryDto {
    @IsString()
    title?: string;

    @IsString()
    category?: string;

    @IsString()
    nutrient?: string;

    @IsString()
    min?: string;

    @IsString()
    max?: string;

    @IsString()
    limit?: string;

    @IsString()
    page?: string;
}
