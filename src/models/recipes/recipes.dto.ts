import { IsString, IsNumber } from 'class-validator';
import { RecipeRO } from './recipe-ro';

export class RecipesDto {

    recipes: RecipeRO[];

    @IsNumber()
    page: number;

    @IsNumber()
    recipesCount: number;

    @IsNumber()
    totalRecipes: number;

    @IsString()
    next: string;

    @IsString()
    previous: string;
}
