import { IsString, IsOptional } from 'class-validator';
import { IngrCreateDTO } from '../ingredients/ingredient-create.dto';
import { SubRecCreateDTO } from '../subrecipes/subrecipe-create.dto';
import { SubRecUpdateDTO } from '../subrecipes/subrecipe-update.dto';
import { IngrUpdateDTO } from '../ingredients/ingredient-update.dto';

export class RecipeUpdateDTO {
    // @IsString()
    // id: string;

    @IsOptional()
    @IsString()
    title?: string;

    @IsOptional()
    @IsString()
    category?: string;

    @IsOptional()
    @IsString()
    imageURL?: string;

    @IsOptional()
    @IsString()
    notes?: string;

    @IsOptional()
    @IsOptional()
    newIngredients?: IngrCreateDTO[];

    @IsOptional()
    @IsOptional()
    newSubrecipes?: SubRecCreateDTO[];

    @IsOptional()
    updateIngredients?: IngrUpdateDTO[];

    @IsOptional()
    updateSubrecipes?: SubRecUpdateDTO[];
}
