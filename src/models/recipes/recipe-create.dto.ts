import { IsString, IsNumber, IsOptional } from 'class-validator';
import { SubRecCreateDTO } from '../subrecipes/subrecipe-create.dto';
import { IngrCreateDTO } from '../ingredients/ingredient-create.dto';

export class RecipeCreateDTO {

    @IsString()
    title: string;

    @IsOptional()
    @IsString()
    imgURL?: string;

    @IsOptional()
    @IsString()
    notes?: string;

    @IsString()
    category: string;

    @IsOptional()
    subRecipes?: SubRecCreateDTO[];

    @IsOptional()
    ingredients?: IngrCreateDTO[];

}
