import { IsString, IsNumber, IsDate } from 'class-validator';
import { IMeasure } from '../../common/interfaces/measure';
import { INutrition } from '../../common/interfaces/nutrition';
import { Subrecipe } from '../../data/entities/subrecipe.entity';
import { Ingredient } from '../../data/entities/ingredient.entity';
import { IngredientRO } from '../ingredients/ingredient-ro.dto';
import { SubrecipeRO } from '../subrecipes/subrecipe-ro.dto';

export class RecipeRO {
    @IsString()
    id: string;

    @IsString()
    title: string;

    @IsString()
    category: string;

    @IsString()
    imgURL: string;

    @IsString()
    notes: string;

    @IsNumber()
    weight: number;

    @IsString()
    unit: string;

    @IsDate()
    createdOn: Date;

    @IsDate()
    updatedOn: Date;

    ingredients?: IngredientRO[];

    subrecipes?: SubrecipeRO[];

    nutrition: INutrition;
}
