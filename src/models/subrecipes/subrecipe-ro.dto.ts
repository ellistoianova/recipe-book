import { IsString, IsNumber } from 'class-validator';
import { IMeasure } from '../../common/interfaces/measure';
import { INutrition } from '../../common/interfaces/nutrition';

export class SubrecipeRO {
    @IsString()
    recipeId: string;

    @IsNumber()
    quantity: number;

    @IsString()
    unit: string;

    @IsString()
    originRecipe: string;

    @IsString()
    nutrition: INutrition;

    @IsNumber()
    gramsPerMeasure: number;
}
