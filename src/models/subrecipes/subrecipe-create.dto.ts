import { IsString, IsNumber } from 'class-validator';

export class SubRecCreateDTO {
    @IsString()
    recipeId: string;

    @IsNumber()
    quantity: number;

    @IsString()
    unit: string;
}
