import { IsString, IsNumber, IsBoolean } from 'class-validator';

export class SubRecUpdateDTO {
    @IsString()
    id: string;

    @IsNumber()
    quantity?: number;

    @IsString()
    unit?: string;

    @IsBoolean()
    isDeleted?: boolean;
}
