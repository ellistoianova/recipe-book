import { IsString, IsNumber } from 'class-validator';
import { IMeasure } from '../../common/interfaces/measure';
import { INutrition } from '../../common/interfaces/nutrition';

export class ProductRO {
    @IsNumber()
    code: number;

    @IsString()
    description: string;

    @IsString()
    foodGroup: string;

    measures: IMeasure[];

    nutrition: INutrition;
}
