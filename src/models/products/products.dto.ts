import { IsString, IsNumber } from 'class-validator';
import { ProductRO } from './product-ro';

export class ProductsDto {

    products: ProductRO[];

    @IsNumber()
    page: number;

    @IsNumber()
    productsCount: number;

    @IsNumber()
    totalProducts: number;

    @IsString()
    next: string;

    @IsString()
    previous: string;
}
