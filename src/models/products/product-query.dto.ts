import { IsString } from 'class-validator';

export class ProductQueryDto {
    @IsString()
    description?: string;

    @IsString()
    foodGroup?: string;

    @IsString()
    limit?: string;

    @IsString()
    page?: string;
}
