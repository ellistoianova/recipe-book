import { IsString, IsNumber } from 'class-validator';

export class IngrCreateDTO {
    @IsNumber()
    code: number;

    @IsNumber()
    quantity: number;

    @IsString()
    unit: string;
}
