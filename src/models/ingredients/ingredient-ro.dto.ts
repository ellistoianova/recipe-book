import { IsString, IsNumber } from 'class-validator';
import { IMeasure } from '../../common/interfaces/measure';
import { INutrition } from '../../common/interfaces/nutrition';

export class IngredientRO {
    @IsString()
    id: string;

    @IsNumber()
    quantity: number;

    @IsString()
    unit: string;

    @IsString()
    product: string;

    measures: IMeasure[];

    @IsString()
    nutrition: INutrition;
}
