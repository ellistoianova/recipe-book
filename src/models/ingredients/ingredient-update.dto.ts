import { IsString, IsNumber, IsBoolean } from 'class-validator';

export class IngrUpdateDTO {
    @IsString()
    id: string;

    @IsNumber()
    quantity?: number;

    @IsString()
    unit?: string;

    @IsBoolean()
    isDeleted?: boolean;
}
