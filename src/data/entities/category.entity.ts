import {
    Entity,
    PrimaryColumn,
    OneToMany,
} from 'typeorm';
import { Recipe } from './recipe.entity';

@Entity('categories')
export class Category {

    @PrimaryColumn()
    name: string;

    @OneToMany(type => Recipe, recipe => recipe.category)
    recipes: Promise<Recipe[]>;
}
