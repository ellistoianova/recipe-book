// tslint:disable-next-line: max-line-length
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, OneToMany, CreateDateColumn, UpdateDateColumn, JoinColumn } from 'typeorm';
import { User } from './user.entity';
import { Nutrition } from './nutrition.entity';
import { Ingredient } from './ingredient.entity';
import { Subrecipe } from './subrecipe.entity';
import { Category } from './category.entity';

@Entity('recipes')
export class Recipe {

@PrimaryGeneratedColumn('uuid')
id: string;

@Column('nvarchar')
title: string;

// tslint:disable-next-line: max-line-length
@Column('longtext', { default: 'https://66.media.tumblr.com/99071f0b45a32c26b90bbfe9a9af2ba4/tumblr_owugniy8K21u9ooogo1_r1_540.gif'})
imageURL: string;

@ManyToOne(type => User, user => user.recipes)
author: Promise<User>;

@OneToMany(type => Subrecipe, subrecipe => subrecipe.currentRecipe, { eager: true })
subrecipes: Subrecipe[];

@OneToMany(type => Subrecipe, subrecipe => subrecipe.originRecipe)
derivedRecipes: Promise<Subrecipe[]>;

@OneToOne(type => Nutrition, nutrition => nutrition.recipe, { eager: true } )
@JoinColumn()
nutrition: Nutrition;

@OneToMany(type => Ingredient, ingredient => ingredient.currentRecipe, { eager: true } )
ingredients: Ingredient[];

@ManyToOne(type => Category, category => category.recipes, { eager: true })
category: Category;

@Column({ default: false })
isDeleted: boolean;

@CreateDateColumn({ type: 'timestamp' })
createdOn: Date;

@UpdateDateColumn({ type: 'timestamp' })
updatedOn?: Date;

@Column('longtext', {default: ''})
notes: string;

// @Column('decimal', { default: 0})
@Column({ default: 0})
weight: number;

@Column({ default: 'g'})
unit: string;

}
