import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Product } from './product.entity';

@Entity('measures')
export class Measure {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Product, product => product.measures)
  product: Promise<Product>;

  @Column()
  measure: string;

  @Column()
  amount: number;

  @Column()
  gramsPerMeasure: number;
}
