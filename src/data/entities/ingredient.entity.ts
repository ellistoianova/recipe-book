import { Entity, Column, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Recipe } from './recipe.entity';
import { Product } from './product.entity';

@Entity('ingredient')
export class Ingredient {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    // @Column('decimal', { default: 0 })
    @Column({ default: 0 })
    quantity: number;

    @Column()
    unit: string;

    @ManyToOne(type => Product, product => product.ingredients, { eager: true })
    product: Product;

    @ManyToOne(type => Recipe, recipe => recipe.ingredients)
    currentRecipe: Promise<Recipe>;

    @Column({ default: false })
    isDeleted: boolean;
}
