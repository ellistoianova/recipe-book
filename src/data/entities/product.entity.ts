import { Entity, PrimaryColumn, Column,  OneToMany, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Measure } from './measure.entity';
import { Nutrition } from './nutrition.entity';
import { Ingredient } from './ingredient.entity';
import { FoodGroup } from './food-group.entity';

@Entity('products')
export class Product {

  @PrimaryColumn()
  code: number;

  @Column('nvarchar')
  description: string;

  @ManyToOne(type => FoodGroup, foodGroup => foodGroup.products, { eager: true })
  foodGroup: FoodGroup;

  @OneToMany(type => Measure, measure => measure.product, { eager: true })
  measures: Measure[];

  @OneToOne(type => Nutrition, nutrition => nutrition.product, { eager: true })
  @JoinColumn()
  nutrition: Nutrition;

  @OneToMany(type => Ingredient, ingredient => ingredient.product)
  ingredients: Promise<Ingredient[]>;

}
