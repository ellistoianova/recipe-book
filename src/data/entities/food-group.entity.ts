import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { Product } from './product.entity';

@Entity('foodGroup')
export class FoodGroup {

    @PrimaryColumn()
    code: number;

    @Column()
    description: string;

    @OneToMany(type => Product, product => product.foodGroup)
    products: Promise<Product[]>;
}
