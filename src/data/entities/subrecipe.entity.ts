import {
    Entity,
    Column,
    ManyToOne,
    PrimaryGeneratedColumn,

} from 'typeorm';

import {
    Recipe,
} from './recipe.entity';

@Entity('subrecipes')
export class Subrecipe {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    // @Column('decimal', { default: 0 })
    @Column({ default: 0 })
    quantity: number;

    @Column()
    unit: string;

    @ManyToOne(type => Recipe, recipe => recipe.subrecipes )
    currentRecipe: Promise<Recipe>;

    @ManyToOne(type => Recipe, recipe => recipe.derivedRecipes)
    originRecipe: Promise<Recipe>;

    @Column({ default: false })
    isDeleted: boolean;
}
