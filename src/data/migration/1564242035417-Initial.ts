import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1564242035417 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `users` (`id` varchar(36) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `joined` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `measures` (`id` varchar(36) NOT NULL, `measure` varchar(255) NOT NULL, `amount` int NOT NULL, `gramsPerMeasure` int NOT NULL, `productCode` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `ingredient` (`id` varchar(36) NOT NULL, `quantity` int NOT NULL DEFAULT 0, `unit` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `productCode` int NULL, `currentRecipeId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `foodGroup` (`code` int NOT NULL, `description` varchar(255) NOT NULL, PRIMARY KEY (`code`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `products` (`code` int NOT NULL, `description` varchar(255) NOT NULL, `foodGroupCode` int NULL, `nutritionId` varchar(36) NULL, UNIQUE INDEX `REL_029502bbd9a8edca9ebb9ae652` (`nutritionId`), PRIMARY KEY (`code`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `nutritions` (`id` varchar(36) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `PROCNT` text NOT NULL, `FAT` text NOT NULL, `CHOCDF` text NOT NULL, `ENERC_KCAL` text NOT NULL, `SUGAR` text NOT NULL, `FIBTG` text NOT NULL, `CA` text NOT NULL, `FE` text NOT NULL, `P` text NOT NULL, `K` text NOT NULL, `NA` text NOT NULL, `VITA_IU` text NOT NULL, `TOCPHA` text NOT NULL, `VITD` text NOT NULL, `VITC` text NOT NULL, `VITB12` text NOT NULL, `FOLAC` text NOT NULL, `CHOLE` text NOT NULL, `FATRN` text NOT NULL, `FASAT` text NOT NULL, `FAMS` text NOT NULL, `FAPU` text NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `subrecipes` (`id` varchar(36) NOT NULL, `quantity` int NOT NULL DEFAULT 0, `unit` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `currentRecipeId` varchar(36) NULL, `originRecipeId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `recipes` (`id` varchar(36) NOT NULL, `title` varchar(255) NOT NULL, `imageURL` longtext NOT NULL DEFAULT 'https://66.media.tumblr.com/99071f0b45a32c26b90bbfe9a9af2ba4/tumblr_owugniy8K21u9ooogo1_r1_540.gif', `isDeleted` tinyint NOT NULL DEFAULT 0, `createdOn` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `notes` longtext NOT NULL DEFAULT '', `weight` int NOT NULL DEFAULT 0, `unit` varchar(255) NOT NULL DEFAULT 'g', `authorId` varchar(36) NULL, `nutritionId` varchar(36) NULL, `categoryName` varchar(255) NULL, UNIQUE INDEX `REL_642147e73deda7e94f8fa87c5b` (`nutritionId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `categories` (`name` varchar(255) NOT NULL, PRIMARY KEY (`name`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `measures` ADD CONSTRAINT `FK_db5edcd1328fb776774cc41420e` FOREIGN KEY (`productCode`) REFERENCES `products`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `ingredient` ADD CONSTRAINT `FK_0488f6c226bcd5bc6fa2eafa3b9` FOREIGN KEY (`productCode`) REFERENCES `products`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `ingredient` ADD CONSTRAINT `FK_f536b1665a76b17808ea641312d` FOREIGN KEY (`currentRecipeId`) REFERENCES `recipes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `products` ADD CONSTRAINT `FK_8e69e50882e0659071cbb19a907` FOREIGN KEY (`foodGroupCode`) REFERENCES `foodGroup`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `products` ADD CONSTRAINT `FK_029502bbd9a8edca9ebb9ae652d` FOREIGN KEY (`nutritionId`) REFERENCES `nutritions`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `subrecipes` ADD CONSTRAINT `FK_676539a9484995037b32e513356` FOREIGN KEY (`currentRecipeId`) REFERENCES `recipes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `subrecipes` ADD CONSTRAINT `FK_36919036938153533d48eb4d071` FOREIGN KEY (`originRecipeId`) REFERENCES `recipes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_afd4f74f8df44df574253a7f37b` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_642147e73deda7e94f8fa87c5bf` FOREIGN KEY (`nutritionId`) REFERENCES `nutritions`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_c5607535ef86a119666d02dc510` FOREIGN KEY (`categoryName`) REFERENCES `categories`(`name`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_c5607535ef86a119666d02dc510`");
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_642147e73deda7e94f8fa87c5bf`");
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_afd4f74f8df44df574253a7f37b`");
        await queryRunner.query("ALTER TABLE `subrecipes` DROP FOREIGN KEY `FK_36919036938153533d48eb4d071`");
        await queryRunner.query("ALTER TABLE `subrecipes` DROP FOREIGN KEY `FK_676539a9484995037b32e513356`");
        await queryRunner.query("ALTER TABLE `products` DROP FOREIGN KEY `FK_029502bbd9a8edca9ebb9ae652d`");
        await queryRunner.query("ALTER TABLE `products` DROP FOREIGN KEY `FK_8e69e50882e0659071cbb19a907`");
        await queryRunner.query("ALTER TABLE `ingredient` DROP FOREIGN KEY `FK_f536b1665a76b17808ea641312d`");
        await queryRunner.query("ALTER TABLE `ingredient` DROP FOREIGN KEY `FK_0488f6c226bcd5bc6fa2eafa3b9`");
        await queryRunner.query("ALTER TABLE `measures` DROP FOREIGN KEY `FK_db5edcd1328fb776774cc41420e`");
        await queryRunner.query("DROP TABLE `categories`");
        await queryRunner.query("DROP INDEX `REL_642147e73deda7e94f8fa87c5b` ON `recipes`");
        await queryRunner.query("DROP TABLE `recipes`");
        await queryRunner.query("DROP TABLE `subrecipes`");
        await queryRunner.query("DROP TABLE `nutritions`");
        await queryRunner.query("DROP INDEX `REL_029502bbd9a8edca9ebb9ae652` ON `products`");
        await queryRunner.query("DROP TABLE `products`");
        await queryRunner.query("DROP TABLE `foodGroup`");
        await queryRunner.query("DROP TABLE `ingredient`");
        await queryRunner.query("DROP TABLE `measures`");
        await queryRunner.query("DROP TABLE `users`");
    }

}
