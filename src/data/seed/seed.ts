import 'reflect-metadata';
import {createConnection} from 'typeorm';
import * as bcrypt from 'bcrypt';

import { User } from '../entities/user.entity';
import { Nutrition } from '../entities/nutrition.entity';
import { Measure } from '../entities/measure.entity';
import { Product } from '../entities/product.entity';

import * as fgroup from '../usda_db/fd_group.json';
import * as fdes from '../usda_db/food_des.json';
import * as nutdata from '../usda_db/nut_data.json';
import * as nutdef from '../usda_db/nutr_def.json';
import * as w from '../usda_db/weight.json';
import * as categ from '../usda_db/category.json';
import { FoodGroup } from '../entities/food-group.entity';
import { Category } from '../entities/category.entity';

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const main = async () => {
    const connection = await createConnection();
    const userRepository = connection.manager.getRepository(User);
    const productRepository = connection.manager.getRepository(Product);
    const nutrientRepository = connection.manager.getRepository(Nutrition);
    const measureRepository = connection.manager.getRepository(Measure);
    const foodgroupRepository = connection.manager.getRepository(FoodGroup);
    const categoryRepository = connection.manager.getRepository(Category);

    const foodGroup = fgroup;
    const foodDes = fdes;
    const nutData = nutdata as any;
    const nutDef = nutdef;
    const weight = w;
    const categories = categ;

    const start = async () => {

    await asyncForEach(foodGroup, async (group) => {
      const foodGp = new FoodGroup();
      foodGp.code = group.FdGrp_Cd;
      foodGp.description = group.FdGrp_desc;
      foodGp.products = Promise.resolve([]);
      await foodgroupRepository.save(foodGp);
    });

    await asyncForEach(categories, async (category) => {
      const cat = new Category();
      cat.name = category.Name;
      cat.recipes = Promise.resolve([]);
      await categoryRepository.save(cat);
    });

    await asyncForEach(foodDes, async (productJSON) => {
      const product = new Product();
      product.code = productJSON.NDB_No;
      product.description = productJSON.Long_Desc;

      const prodFoodGroup = productJSON.FdGrp_Cd;
      const foodGp = await foodgroupRepository.findOne({where: { code: prodFoodGroup} });
      product.foodGroup = foodGp;

      await productRepository.save(product);

      const measureInGram = new Measure();
      measureInGram.amount = 1;
      measureInGram.gramsPerMeasure = 1;
      measureInGram.measure = 'g';
      measureInGram.product = Promise.resolve(product);
      await measureRepository.save(measureInGram);

      const measures = weight.filter(measure => measure.NDB_No === product.code);

      await asyncForEach(measures, async (measureItem) => {
        const measure = new Measure();

        measure.measure = measureItem.Msre_Desc;
        let amount = measureItem.Amount;
        if (measureItem.Msre_Desc.includes('package') && amount !== 1) {
          amount = 1;
        }
        measure.gramsPerMeasure = measureItem.Gm_Wgt;
        measure.amount = amount;

        measure.product = Promise.resolve(product);
        await measureRepository.save(measure);

      });

      const nutrition = new Nutrition();

      nutDef.forEach(nutrient => {
        const nutrCode = nutrient.Nutr_no;
        let nutValue;
        const nutValueItem = nutData.find((item) => (item.NDB_No === product.code && item.Nutr_No === nutrCode));
        if (nutValueItem === undefined) {
          nutValue = 0;
        } else {
          nutValue = nutValueItem.Nutr_Val;
        }
        nutrition[nutrient.Tagname] = {
          description: nutrient.NutrDesc,
          unit: nutrient.Units,
          value: nutValue,
        };

      });
      nutrition.product = Promise.resolve(product);
      nutrition.recipe = Promise.resolve(null);
      await nutrientRepository.save(nutrition);

    });
    const testChef = await userRepository.findOne({
        where: {
          firstName: 'Eli',
        },
      });

    if (!testChef) {
        const user = new User();
        user.username = 'testChefEli';
        const passwordHash = await bcrypt.hash('D02e9050!', 10);
        user.password = passwordHash;
        user.email = 'elli.debelli@gmail.com';
        user.firstName = 'Eli';
        user.lastName = 'Debelli';
        await userRepository.save(user);
      } else {
        // tslint:disable-next-line: no-console
        console.log(`Eli already in the db`);
      }

    connection.close();
  };
    start();
};

// tslint:disable-next-line: no-console
main().catch(console.error);
