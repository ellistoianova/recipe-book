export interface IItem {
    quantity: number;
    unit: string;
}
