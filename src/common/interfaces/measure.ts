export interface IMeasure {
    measure: string;
    amount: number;
    gramsPerMeasure: number;
}
