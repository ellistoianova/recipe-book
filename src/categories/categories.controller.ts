import { Controller, Get } from '@nestjs/common';
import { Category } from '../data/entities/category.entity';
import { CategoriesService } from './categories.service';

@Controller('api/categories')
export class CategoriesController {

   constructor(
        private readonly categoriesService: CategoriesService,
    ) { }

    @Get()
    async getAllCategories(): Promise<Category[]> {
        return await this.categoriesService.getAllCategories();
    }
}
