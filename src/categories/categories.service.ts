import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from '../data/entities/category.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CategoriesService {

    constructor(@InjectRepository(Category) private readonly categoriesRepository: Repository<Category>) { }

    async getAllCategories(): Promise<Category[]> {
        return await this.categoriesRepository.find();
    }
}
