import { Controller, UseFilters, UseGuards, Get, Query, Param } from '@nestjs/common';

// import { AuthGuardWithBlackisting } from '../common/guards/custom-auth.guard';
import { ProductsService } from './products.service';
import { ProductQueryDto } from '../models/products/product-query.dto';
import { NotFoundFilter } from '../common/filters/not-found.filter';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard('jwt'))
@Controller('api/products')
export class ProductsController {
    constructor(private readonly productsService: ProductsService) { }

    @Get()
    @UseFilters(NotFoundFilter)
    async getProducts(@Query() query: ProductQueryDto) {
        const route: string = `localhost:3000/api/products`;
        return this.productsService.getProducts(query, route);
    }
    @Get(':code')
    @UseFilters(NotFoundFilter)
    async getSingleProduct(@Param('code') code: string) {
        return this.productsService.findProductByCode(+code);
    }
}
