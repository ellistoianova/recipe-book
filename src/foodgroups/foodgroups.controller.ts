import { Controller, Get } from '@nestjs/common';
import { FoodGroup } from '../data/entities/food-group.entity';
import { FoodgroupsService } from './foodgroups.service';

@Controller('api/foodgroups')
export class FoodgroupsController {

    constructor(
        private readonly foodgroupsService: FoodgroupsService,
    ) { }

    @Get()
    async all(): Promise<FoodGroup[]> {
        return await this.foodgroupsService.getAllFoodGroups();
    }
}
