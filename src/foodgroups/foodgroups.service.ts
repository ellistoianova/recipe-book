import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FoodGroup } from '../data/entities/food-group.entity';
import { Repository } from 'typeorm';

@Injectable()
export class FoodgroupsService {
    constructor(@InjectRepository(FoodGroup) private readonly foodgroupRepository: Repository<FoodGroup>) { }

    async getAllFoodGroups(): Promise<FoodGroup[]> {
        return await this.foodgroupRepository.find();
    }
}
