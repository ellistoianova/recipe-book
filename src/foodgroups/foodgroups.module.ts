import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { FoodGroup } from '../data/entities/food-group.entity';
import { FoodgroupsController } from './foodgroups.controller';
import { FoodgroupsService } from './foodgroups.service';

@Module({
  imports: [AuthModule, TypeOrmModule.forFeature([FoodGroup])],
  controllers: [FoodgroupsController],
  providers: [FoodgroupsService],
})
export class FoodgroupsModule { }
