import { Recipe } from './../data/entities/recipe.entity';
import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../data/entities/user.entity';
import { Nutrition } from '../data/entities/nutrition.entity';
import { IngrCreateDTO } from '../models/ingredients/ingredient-create.dto';
import { IngredientService } from '../helpers/ingredient.service';
import { NutritionService } from '../helpers/nutrition.service';
import { INutrition } from '../common/interfaces/nutrition';
import { SubRecCreateDTO } from '../models/subrecipes/subrecipe-create.dto';
import { SubrecipeService } from '../helpers/subrecipe.service';
import { CategoryService } from '../helpers/category.service';
import { IngrUpdateDTO } from '../models/ingredients/ingredient-update.dto';
import { SubRecUpdateDTO } from '../models/subrecipes/subrecipe-update.dto';
import { RecipeQueryDto } from '../models/recipes/recipe-query.dto';
import { RecipesDto } from '../models/recipes/recipes.dto';
import { RecipeRO } from '../models/recipes/recipe-ro';
import { IngredientRO } from '../models/ingredients/ingredient-ro.dto';
import { IMeasure } from '../common/interfaces/measure';

@Injectable()
export class RecipesService {
    constructor(
        @InjectRepository(Recipe) private readonly recipeRepository: Repository<Recipe>,
        @InjectRepository(Nutrition) private readonly nutritionRepository: Repository<Nutrition>,
        private readonly ingredientService: IngredientService,
        private readonly nutritionService: NutritionService,
        private readonly subrecipeService: SubrecipeService,
        private readonly categoryService: CategoryService,
    ) { }

    async getRecipes(query: RecipeQueryDto, route: string, user: User): Promise<RecipesDto> {
        const title = query.title ? query.title : '';
        const category = query.category ? query.category : '';
        const nutrient = query.nutrient ? query.nutrient : '';
        const min = query.min ? query.min : 0;
        const max = query.max ? query.max : 0;
        const author = user;

        let limit = query.limit ? +query.limit : 0;
        limit = limit > 100 ? 100 : limit;
        let page = query.page ? +query.page : 1;
        page = page < 0 ? 1 : page;
        let queryStr = `${route}?`;

        const queryBuilder = await this.recipeRepository
            .createQueryBuilder('recipe')
            .leftJoinAndSelect('recipe.category', 'category')
            .leftJoinAndSelect('recipe.nutrition', 'nutrition')
            .addOrderBy('recipe.title', 'ASC')
            .innerJoin('recipe.author', 'author', 'author.username = :username', { username: user.username })
            .where('recipe.isDeleted = :isDeleted', { isDeleted: false });

        if (title) {
            queryBuilder.andWhere('LOWER(recipe.title) LIKE :title', {
                title: `%${title.toLowerCase()}%`,
            });
            queryStr = queryStr.concat(`title=${title}&`);
        }

        if (category) {
            queryBuilder.andWhere('LOWER(recipe.category) LIKE :category', {
                category: `%${category.toLowerCase()}%`,
            });
            queryStr = queryStr.concat(`category=${category}&`);
        }

        const recipes = await queryBuilder.getMany();

        let filteredRecipes = recipes;
        // if (nutrient) {
        //     if (min && max) {
        //         filteredRecipes = recipes.filter((recipe) => recipe.nutrition[nutrient].value >= +min && recipe.nutrition[nutrient].value <= +max);
        //     } else if (min) {
        //     filteredRecipes = recipes.filter((recipe) => recipe.nutrition[nutrient].value >= +min);
        //     } else if (max) {
        //     filteredRecipes = recipes.filter((recipe) => recipe.nutrition[nutrient].value <= +max);
        //     }
        // }
        if (nutrient) {
            if (min && max) {
                // tslint:disable-next-line: max-line-length
                filteredRecipes = recipes.filter((recipe) => recipe.nutrition[nutrient].value / 100 * recipe.weight >= +min && recipe.nutrition[nutrient].value / 100 * recipe.weight <= +max);
            } else if (min) {
                filteredRecipes = recipes.filter((recipe) => recipe.nutrition[nutrient].value / 100 * recipe.weight >= +min);
            } else if (max) {
                filteredRecipes = recipes.filter((recipe) => recipe.nutrition[nutrient].value / 100 * recipe.weight <= +max);
            }
        }

        const recipesROArr = await Promise.all(filteredRecipes.map((recipe) => this.recipeToRO(recipe, false))).then(result => result);

        const total = recipesROArr.length;
        const isNext = limit ? route && (total / limit >= page) : false;
        const isPrevious = route && page > 1;
        const recipesToReturn = new RecipesDto();
        const itemsToShow = limit === 0 ? total : limit;
        const pageToVisit = page > Math.ceil(total / limit) ? Math.ceil(total / limit) : page;
        recipesToReturn.recipes = this.paginate(recipesROArr, pageToVisit, itemsToShow);
        recipesToReturn.page = pageToVisit;
        recipesToReturn.recipesCount = total < limit || limit === 0 ? total : recipesToReturn.recipes.length;
        recipesToReturn.totalRecipes = total;
        recipesToReturn.next = isNext ? `${queryStr}page=${page + 1}` : '';
        recipesToReturn.previous = isPrevious ? `${queryStr}page=${page - 1}` : '';

        return recipesToReturn;
    }

    private paginate(totalRecipes, page, itemsToShow) {
        const startIndex = (page - 1) * itemsToShow;
        let endIndex = startIndex + itemsToShow;
        if (endIndex > totalRecipes.length) {
            endIndex = totalRecipes.length;
        }

        const result = totalRecipes.slice(startIndex, endIndex);
        return result;
    }

    private async recipeToRO(recipe: Recipe, findOne: boolean): Promise<RecipeRO> {
         const nutrition: INutrition = {
            PROCNT: recipe.nutrition.PROCNT,
            FAT: recipe.nutrition.FAT,
            CHOCDF: recipe.nutrition.CHOCDF,
            ENERC_KCAL: recipe.nutrition.ENERC_KCAL,
            SUGAR: recipe.nutrition.SUGAR,
            FIBTG: recipe.nutrition.FIBTG,
            CA: recipe.nutrition.CA,
            FE: recipe.nutrition.FE,
            P: recipe.nutrition.P,
            K: recipe.nutrition.K,
            NA: recipe.nutrition.NA,
            VITA_IU: recipe.nutrition.VITA_IU,
            TOCPHA: recipe.nutrition.TOCPHA,
            VITD: recipe.nutrition.VITD,
            VITC: recipe.nutrition.VITC,
            VITB12: recipe.nutrition.VITB12,
            FOLAC: recipe.nutrition.FOLAC,
            CHOLE: recipe.nutrition.CHOLE,
            FATRN: recipe.nutrition.FATRN,
            FASAT: recipe.nutrition.FASAT,
            FAMS: recipe.nutrition.FAMS,
            FAPU: recipe.nutrition.FAPU,
        };
         const recipeRO: RecipeRO = {
            id: recipe.id,
            title: recipe.title,
            imgURL: recipe.imageURL,
            category: recipe.category.name,
            notes: recipe.notes,
            weight: recipe.weight,
            unit: recipe.unit,
            createdOn: recipe.createdOn,
            updatedOn: recipe.updatedOn,
            nutrition,
        };
         if (findOne) {
            const ingredients = recipe.ingredients.filter((ingredient) => ingredient.isDeleted === false) ;

            recipeRO.ingredients = ingredients.map((ingredient) => {

                const productNutrition: INutrition = {
                    PROCNT: ingredient.product.nutrition.PROCNT,
                    FAT: ingredient.product.nutrition.FAT,
                    CHOCDF: ingredient.product.nutrition.CHOCDF,
                    ENERC_KCAL: ingredient.product.nutrition.ENERC_KCAL,
                    SUGAR: ingredient.product.nutrition.SUGAR,
                    FIBTG: ingredient.product.nutrition.FIBTG,
                    CA: ingredient.product.nutrition.CA,
                    FE: ingredient.product.nutrition.FE,
                    P: ingredient.product.nutrition.P,
                    K: ingredient.product.nutrition.K,
                    NA: ingredient.product.nutrition.NA,
                    VITA_IU: ingredient.product.nutrition.VITA_IU,
                    TOCPHA: ingredient.product.nutrition.TOCPHA,
                    VITD: ingredient.product.nutrition.VITD,
                    VITC: ingredient.product.nutrition.VITC,
                    VITB12: ingredient.product.nutrition.VITB12,
                    FOLAC: ingredient.product.nutrition.FOLAC,
                    CHOLE: ingredient.product.nutrition.CHOLE,
                    FATRN: ingredient.product.nutrition.FATRN,
                    FASAT: ingredient.product.nutrition.FASAT,
                    FAMS: ingredient.product.nutrition.FAMS,
                    FAPU: ingredient.product.nutrition.FAPU,
                };
                const measures: IMeasure[] = ingredient.product.measures.map((msr) => {
                    const measureToReturn: IMeasure = {
                        measure: msr.measure,
                        amount: msr.amount,
                        gramsPerMeasure: msr.gramsPerMeasure,
                    };

                    return measureToReturn;
                });
                const ingredientRO: IngredientRO = {
                    id: ingredient.id,
                    quantity: ingredient.quantity,
                    unit: ingredient.unit,
                    product: ingredient.product.description,
                    measures,
                    nutrition: productNutrition,
                };
                return ingredientRO;
            });
            const subrecipes = recipe.subrecipes.filter((subrecipe) => subrecipe.isDeleted === false);
            recipeRO.subrecipes = await Promise.all(subrecipes.map(async (subrecipe) => {

                const originRecipeId = (await subrecipe.originRecipe).id;
                const originrecipe = await this.getRecipeByID(originRecipeId);
                const originRecNutrition = await originrecipe.nutrition;
                const subrecipeNutrition: INutrition = {
                    PROCNT: originRecNutrition.PROCNT,
                    FAT: originRecNutrition.FAT,
                    CHOCDF: originRecNutrition.CHOCDF,
                    ENERC_KCAL: originRecNutrition.ENERC_KCAL,
                    SUGAR: originRecNutrition.SUGAR,
                    FIBTG: originRecNutrition.FIBTG,
                    CA: originRecNutrition.CA,
                    FE: originRecNutrition.FE,
                    P: originRecNutrition.P,
                    K: originRecNutrition.K,
                    NA: originRecNutrition.NA,
                    VITA_IU: originRecNutrition.VITA_IU,
                    TOCPHA: originRecNutrition.TOCPHA,
                    VITD: originRecNutrition.VITD,
                    VITC: originRecNutrition.VITC,
                    VITB12: originRecNutrition.VITB12,
                    FOLAC: originRecNutrition.FOLAC,
                    CHOLE: originRecNutrition.CHOLE,
                    FATRN: originRecNutrition.FATRN,
                    FASAT: originRecNutrition.FASAT,
                    FAMS: originRecNutrition.FAMS,
                    FAPU: originRecNutrition.FAPU,
                };
                const subrecipeRO = {
                    recipeId: subrecipe.id,
                    quantity: subrecipe.quantity,
                    unit: subrecipe.unit,
                    originRecipe: originrecipe.title,
                    nutrition: subrecipeNutrition,
                    gramsPerMeasure: +originrecipe.weight,
                };
                return subrecipeRO;
            })).then(result => result);
        }

         return recipeRO;
    }
async validateIfRecipeExists(title: string) {
    const foundRecipe = await this.recipeRepository.findOne({ where: { title, isDeleted: false } });
    if (foundRecipe) {
        throw new BadRequestException('Recipe with such title already exists!');
    }
}
// tslint:disable-next-line: max-line-length
async createRecipe(ingredientObjects: IngrCreateDTO[], subRecObjects: SubRecCreateDTO[], user: User, title: string, imageURL: string, catName: string, notes: string ): Promise<{ message: string, id: string }> {
    const recipe = new Recipe();
    await this.validateIfRecipeExists(title);
    recipe.title = title;
    recipe.author = Promise.resolve(user);
    if (imageURL) {
            recipe.imageURL = imageURL;
        }
    if (notes) {
            recipe.notes = notes;
        }
    const category = await this.categoryService.getCategoryByName(catName);
    recipe.category = category;
    recipe.derivedRecipes = Promise.resolve([]);
    recipe.weight = 0;
    recipe.unit = 'g';
    await this.recipeRepository.save(recipe);
    let totalWeight = 0;
    const nutritionArr = [];

    if (ingredientObjects) {
        const ingredients = await Promise.all(ingredientObjects.map(async (ingredientObj) => {
            // return await this.ingredientService.createIngredient(ingredientObj, recipe);
            const newIngredient = await this.ingredientService.createNewIngredient(ingredientObj, recipe);
            return await this.ingredientService.calculateHelper(newIngredient);

        })).then(result => result);
        ingredients.forEach((ingredient) => {
            totalWeight += ingredient.weightInGrams;
            nutritionArr.push(ingredient.totalNutritionValue);
        });
    }

    if (subRecObjects) {
        const subRecipes = await Promise.all(subRecObjects.map(async (subRecObject) => {
            const originalRec = await this.getRecipeByID(subRecObject.recipeId);
            // return await this.subrecipeService.createSubRec(subRecObject, recipe, originalRec);
            const newSubrecipe = await this.subrecipeService.createNewSubrecipe(subRecObject, originalRec, recipe);
            // recipe.subrecipes = [...await recipe.subrecipes, newSubrecipe];
            return await this.subrecipeService.calculateHelper(newSubrecipe, originalRec);
        })).then(result => result);
        subRecipes.forEach((subrecipe) => {
            totalWeight += subrecipe.weightInGrams;
            nutritionArr.push(subrecipe.totalNutritionValue);
        });
    }

    const totalNutritionOfRecipe: INutrition = this.nutritionService.sumNutritionValues(nutritionArr);

    recipe.weight = totalWeight;

    const nutritionPer100Grams = await this.nutritionService.calculateRecNutrPer100Grams(totalWeight, totalNutritionOfRecipe);
    const nutritionToBeCreated = { ...nutritionPer100Grams, product: null };
    const createdNutrition = await this.nutritionRepository.create(nutritionToBeCreated);
    const finalCreatedNutrition = await this.nutritionRepository.save(createdNutrition);
    recipe.nutrition = finalCreatedNutrition;

    const savedRecipe = await this.recipeRepository.save(recipe);
    return { message: 'Recipe successfully created!', id: savedRecipe.id };
}

    async getRecipeByID( recipeID: string ): Promise<Recipe> {
        const recipe = await this.recipeRepository.findOne({ where: { id: recipeID, isDeleted: false }});
        if (!recipe) {
            throw new NotFoundException('Such a recipe does not exist or has been deleted!');
        }
        return recipe;
    }
    async checkUseraAndGetRecipe( recipeID: string, user: User ): Promise<RecipeRO> {

        const recipe = await this.recipeRepository.findOne({ where: { id: recipeID, isDeleted: false, author: user }});
        if (!recipe) {
            throw new NotFoundException('Recipe does not exist or does not belong to this author');
        }
        const recipeRO = await this.recipeToRO(recipe, true);
        return recipeRO;
    }

    async asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
        }
    }

    async deleteRecipeById(recipeId): Promise<{ message: string }> {
        const recipe = await this.getRecipeByID(recipeId);

        const derivedRecipes = await recipe.derivedRecipes;
        if (derivedRecipes.length > 0) {
            const hasRecipes = derivedRecipes.some((derivedRecipe) => derivedRecipe.isDeleted === false);
            if (hasRecipes) {
                throw new BadRequestException('This recipe takes part as an ingredient in other recipes and cannot be deleted!');
            }
        }
        const start = async () => {
            const updateField = { isDeleted: true };
            await this.recipeRepository.update(recipe.id, updateField);

            const ingredients = recipe.ingredients;
            if (ingredients.length > 0) {
                await this.asyncForEach(ingredients, async (ingredient) => {
                    await this.ingredientService.deleteIngredientById(ingredient.id);
                });
            }
            const subrecipes = recipe.subrecipes;
            if (subrecipes.length > 0) {
                await this.asyncForEach(subrecipes, async (subrecipe) => {
                    await this.subrecipeService.deleteSubrecipeById(subrecipe.id);
                });
            }
            const nutrition = recipe.nutrition;
            await this.nutritionService.deleteNutritionById(nutrition.id);
        };
        start();
        return { message: 'Recipe successfully deleted' };
    }

    async updateRecipeById(recipeId, data): Promise<{ message: string }> {
        const recipe: Recipe = await this.getRecipeByID(recipeId);
        if (data.title) {
            recipe.title = data.title;
        }
        if (data.category) {
            const category = await this.categoryService.getCategoryByName(data.category);
            recipe.category = category;
        }
        if (data.imageURL) {
            recipe.imageURL = data.imageURL;
        }
        if (data.notes) {
            recipe.notes = data.notes;
        }
        await this.recipeRepository.save(recipe);

        if (data.newIngredients || data.newSubrecipes || data.updateIngredients || data.updateSubrecipes) {

            let totalWeight = 0;
            const nutritionArr = [];

            if (data.newIngredients) {
                    const ingredientsToCreate: IngrCreateDTO[] = data.newIngredients;
                    const ingredients = await Promise.all(ingredientsToCreate.map(async (ingredient) => {
                        const newIngredient = await this.ingredientService.createNewIngredient(ingredient, recipe);
                        recipe.ingredients = [...await recipe.ingredients, newIngredient];
                        return await this.ingredientService.calculateHelper(newIngredient);
                    })).then(result => result);

                }
            if (data.newSubrecipes) {
                    const subRecObjects: SubRecCreateDTO[] = data.newSubrecipes;
                    const subRecipes = await Promise.all(subRecObjects.map(async (subRecObject) => {
                        const originalRec = await this.getRecipeByID(subRecObject.recipeId);
                        const newSubrecipe = await this.subrecipeService.createNewSubrecipe(subRecObject, originalRec, recipe);
                        recipe.subrecipes = [...await recipe.subrecipes, newSubrecipe];
                        return await this.subrecipeService.calculateHelper(newSubrecipe, originalRec);
                    })).then(result => result);

                }
            if (data.updateIngredients) {
                const ingredientsToUpdate: IngrUpdateDTO[] = data.updateIngredients;
                await Promise.all(ingredientsToUpdate.map(async (ingredient: IngrUpdateDTO) => {
                    const updateIngredient = await this.ingredientService.updateIngredientById(ingredient, recipe);
                    return updateIngredient;
                }));
            }

            if (data.updateSubrecipes) {
                const subrecipesToUpdate: SubRecUpdateDTO[] = data.updateSubrecipes;
                await Promise.all(subrecipesToUpdate.map(async (subrecipe: SubRecUpdateDTO) => {
                    return await this.subrecipeService.updateSubrecipeById(subrecipe);
                }));
            }
            const updateRecipe = await this.getRecipeByID(recipeId);

            let ingredientsLeft = [];
            let subrecipesLeft = [];
            if (updateRecipe.ingredients) {
                ingredientsLeft = updateRecipe.ingredients.filter((ingredient) => {
                    return ingredient.isDeleted === false;
                });
            }
            if (updateRecipe.subrecipes) {
                subrecipesLeft = updateRecipe.subrecipes.filter((subrecipe) => {
                    return subrecipe.isDeleted === false;
                });
            }
            if (ingredientsLeft.length === 0 && subrecipesLeft.length === 0) {
                throw new BadRequestException('You cannot delete all ingredients and subrecipes when editing a recipe!');
            }

            if (updateRecipe.ingredients) {
                const ingredients = await updateRecipe.ingredients;
                const ingredientsToCalculate = ingredients.filter((ingredient) => {
                    return ingredient.isDeleted === false;
                });
                if (ingredientsToCalculate.length > 0) {
                    const updatedIngredients = await Promise.all(ingredientsToCalculate.map(async (ingredient) => {
                        return await this.ingredientService.calculateHelper(ingredient);
                    })).then(result => result);
                    updatedIngredients.forEach((updatedIngredient) => {
                        totalWeight += updatedIngredient.weightInGrams;
                        nutritionArr.push(updatedIngredient.totalNutritionValue);
                    });
                }
            }

            if (updateRecipe.subrecipes) {
                const subrecipes = updateRecipe.subrecipes;
                const subrecipesToCalculate = subrecipes.filter((subrecipe) => {
                    return subrecipe.isDeleted === false;
                });
                if (subrecipesToCalculate.length > 0) {
                    const updatedSubrecipes = await Promise.all(subrecipesToCalculate.map(async (subrecipe) => {
                        const originalRecId = (await subrecipe.originRecipe).id;
                        const originalRec = await this.getRecipeByID(originalRecId);
                        return await this.subrecipeService.calculateHelper(subrecipe, originalRec);
                    })).then(result => result);
                    updatedSubrecipes.forEach((updatedSubrecipe) => {
                        totalWeight += updatedSubrecipe.weightInGrams;
                        nutritionArr.push(updatedSubrecipe.totalNutritionValue);
                    });
                }
            }

            const totalNutritionOfRecipe: INutrition = this.nutritionService.sumNutritionValues(nutritionArr);

            updateRecipe.weight = totalWeight;
            const nutrition = await updateRecipe.nutrition;
            const nutritionPer100Grams = await this.nutritionService.calculateRecNutrPer100Grams(totalWeight, totalNutritionOfRecipe);
            const updatedNutrition = await this.nutritionService.updateNutritionById(nutrition, nutritionPer100Grams);

            recipe.nutrition = updatedNutrition;
            await this.recipeRepository.save(updateRecipe);
            return { message: 'Recipe successfully updated!' };
        }
        return { message: 'Recipe successfully updated!' };
    }
}
