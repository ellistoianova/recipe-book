import { RecipesService } from './recipes.service';
import { Controller, Body, Post, Req, UseGuards, Param, Get, Delete, Put, Query, ValidationPipe, UseFilters } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RecipeCreateDTO } from '../models/recipes/recipe-create.dto';
import { RecipeUpdateDTO } from '../models/recipes/recipe-update.dto';
import { RecipeQueryDto } from '../models/recipes/recipe-query.dto';
import { User as Userdec } from '../decorators/user.decorator';
import { User } from '../data/entities/user.entity';
import { RecipeRO } from '../models/recipes/recipe-ro';
import { NotFoundFilter } from '../common/filters/not-found.filter';
import { BadRequestFilter } from '../common/filters/bad-request.filter';

// @UseGuards(AuthGuardWithBlackisting)
@UseGuards(AuthGuard('jwt'))
@Controller('api/recipes')
export class RecipesController {
  constructor(private readonly resipesService: RecipesService) { }

  @Get()
  @UseFilters(NotFoundFilter)
  async getRecipes(@Query() query: RecipeQueryDto, @Userdec() user: User) {
    const route: string = `localhost:3000/api/recipes`;
    return this.resipesService.getRecipes(query, route, user);
  }

  @Get(':id')
  @UseFilters(NotFoundFilter)
  async getSingleRecipe(@Param('id') id: string, @Userdec() user: User): Promise<RecipeRO> {
    return await this.resipesService.checkUseraAndGetRecipe(id, user);
  }

  @Post()
  @UseFilters(NotFoundFilter, BadRequestFilter)
  // tslint:disable-next-line: max-line-length
  async createRecipe(@Body(new ValidationPipe({ whitelist: true, transform: true })) data: RecipeCreateDTO, @Userdec() user: User): Promise<{ message: string, id: string }> {
    // tslint:disable-next-line: max-line-length
    return await this.resipesService.createRecipe(data.ingredients, data.subRecipes, user, data.title, data.imgURL, data.category, data.notes);
  }

  @Put(':id')
  @UseFilters(NotFoundFilter, BadRequestFilter)
  // tslint:disable-next-line: max-line-length
  async updateSingleRecipe(@Param('id') id: string, @Body(new ValidationPipe({ whitelist: true, transform: true })) data: RecipeUpdateDTO): Promise<{ message: string }> {
    return await this.resipesService.updateRecipeById(id, data);
  }
  @Delete(':id')
  @UseFilters(NotFoundFilter, BadRequestFilter)
  async deleteSingleRecipe(@Param('id') id: string): Promise<{ message: string }> {
    return await this.resipesService.deleteRecipeById(id);
  }

}
