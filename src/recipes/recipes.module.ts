import { Product } from './../data/entities/product.entity';

import { Module } from '@nestjs/common';
import { RecipesController } from './recipes.controller';
import { RecipesService } from './recipes.service';
import { Recipe } from '../data/entities/recipe.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Subrecipe } from '../data/entities/subrecipe.entity';
import { User } from '../data/entities/user.entity';
import { Nutrition } from '../data/entities/nutrition.entity';
import { Ingredient } from '../data/entities/ingredient.entity';
import { IngredientService } from '../helpers/ingredient.service';
import { SubrecipeService } from '../helpers/subrecipe.service';
import { NutritionService } from '../helpers/nutrition.service';
import { AuthModule } from '../auth/auth.module';
import { FoodGroupService } from '../helpers/foodgroup.service';
import { ProductsModule } from '../products/products.module';
import { ProductsService } from '../products/products.service';
import { FoodGroup } from '../data/entities/food-group.entity';
import { Category } from '../data/entities/category.entity';
import { CategoryService } from '../helpers/category.service';

@Module({
  imports: [TypeOrmModule.forFeature([Recipe, Ingredient, Product, Subrecipe, User, Nutrition, FoodGroup, Category]), AuthModule, ProductsModule],
  controllers: [RecipesController],
  providers: [RecipesService, IngredientService, SubrecipeService, NutritionService, FoodGroupService, ProductsService, CategoryService],
})
export class RecipesModule { }
