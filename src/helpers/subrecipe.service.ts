import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ingredient } from '../data/entities/ingredient.entity';
import { Repository, UpdateResult } from 'typeorm';
import { Subrecipe } from '../data/entities/subrecipe.entity';
import { SubRecCreateDTO } from '../models/subrecipes/subrecipe-create.dto';
import { Recipe } from '../data/entities/recipe.entity';
import { RecipesService } from '../recipes/recipes.service';
import { NutritionService } from './nutrition.service';
import { SubRecUpdateDTO } from '../models/subrecipes/subrecipe-update.dto';
import { BadRequestException } from '../common/exeptions/bad-request';

@Injectable()
export class SubrecipeService {
    constructor(
        @InjectRepository(Subrecipe) private readonly subrecipeRepo: Repository<Subrecipe>,
        private readonly nutritionService: NutritionService) {}

    // async createSubRec(subrecObj: SubRecCreateDTO, recipe: Recipe, originRec: Recipe) {

    //     const subrecipe = new Subrecipe();

    //     subrecipe.originRecipe = Promise.resolve(originRec);
    //     subrecipe.currentRecipe =  Promise.resolve(recipe);
    //     subrecipe.quantity = subrecObj.quantity;
    //     subrecipe.unit = subrecObj.unit;

    //     await this.subrecipeRepo.save(subrecipe);
    //     const weightInGrams = subrecObj.quantity * originRec.weight;
    //     const totalNutritionValue = await this.nutritionService.calculateNutrition(weightInGrams, originRec.nutrition);

    //     return {
    //         weightInGrams,
    //         totalNutritionValue,
    //     };
    // }

    async createNewSubrecipe(subrecObj: SubRecCreateDTO, originRec: Recipe, recipe: Recipe): Promise<Subrecipe> {
        const subrecipe = new Subrecipe();

        subrecipe.originRecipe = Promise.resolve(originRec);
        subrecipe.currentRecipe = Promise.resolve(recipe);
        subrecipe.quantity = subrecObj.quantity;
        subrecipe.unit = subrecObj.unit;

        return await this.subrecipeRepo.save(subrecipe);
    }
    async calculateHelper(subrecipe: Subrecipe, originRec: Recipe) {

        const weightInGrams = subrecipe.quantity * originRec.weight;
        const originRecNutrition = originRec.nutrition;
        const totalNutritionValue = await this.nutritionService.calculateNutrition(weightInGrams, originRecNutrition);

        return {
            weightInGrams,
            totalNutritionValue,
        };
    }

    async getSubrecipeById(subrecipeId: string): Promise<Subrecipe> {
        const subrecipe = await this.subrecipeRepo.findOne({ where: { id: subrecipeId } });
        // if (!subrecipe) {
        //     throw new NotFoundException();
        // }
        return subrecipe;
    }
    async deleteSubrecipeById(subrecipeId: string) {
        const subrecipeToDelete = await this.getSubrecipeById(subrecipeId);
        const updatedField = { isDeleted: true };
        if (subrecipeToDelete.isDeleted === false) {
        return await this.subrecipeRepo.update(subrecipeToDelete.id, updatedField);
        }
    }
    async updateSubrecipeById(subrecipeToEdit: SubRecUpdateDTO): Promise<Subrecipe> {
        const subrecipe = await this.getSubrecipeById(subrecipeToEdit.id);
        if (!subrecipe) {
            throw new NotFoundException('Such recipe cannot be found.');
        }
        if (subrecipe.isDeleted === true) {
            throw new BadRequestException('This subrecipe does not exist!');
        }
        if (subrecipeToEdit.unit) {
            subrecipe.unit = subrecipeToEdit.unit;
        }
        if (subrecipeToEdit.quantity) {
            subrecipe.quantity = subrecipeToEdit.quantity;
        }
        if (subrecipeToEdit.isDeleted) {
            subrecipe.isDeleted = subrecipeToEdit.isDeleted;
        }
        return await this.subrecipeRepo.save(subrecipe);
    }
}
