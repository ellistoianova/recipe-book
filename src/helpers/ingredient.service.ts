import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ingredient } from '../data/entities/ingredient.entity';
import { Repository } from 'typeorm';
import { IngrCreateDTO } from '../models/ingredients/ingredient-create.dto';
import { ProductsService } from '../products/products.service';
import { Recipe } from '../data/entities/recipe.entity';
import { NutritionService } from './nutrition.service';
import { IngrUpdateDTO } from '../models/ingredients/ingredient-update.dto';
import { BadRequestException } from '../common/exeptions/bad-request';

@Injectable()
export class IngredientService {
    constructor(@InjectRepository(Ingredient) private readonly ingredientRepo: Repository<Ingredient>,
                private readonly productService: ProductsService,
                private readonly nutritionService: NutritionService,
    ) { }

    // async createIngredient(ingredientObj: IngrCreateDTO, recipe: Recipe) {
    //     const ingredient = new Ingredient();
    //     const originProduct = await this.productService.findProductByCode(ingredientObj.code);
    //     ingredient.product = originProduct;
    //     ingredient.quantity = ingredientObj.quantity;
    //     ingredient.unit = ingredientObj.unit;

    //     ingredient.currentRecipe = Promise.resolve(recipe);

    //     await this.ingredientRepo.save(ingredient);

    //     const measureOfIngredient = originProduct.measures.find((measure) => {
    //         return `${measure.amount} ${measure.measure}` === ingredientObj.unit;
    //     });
    //     const weightInGrams = measureOfIngredient.gramsPerMeasure * ingredientObj.quantity;

    //     const totalNutritionValue = await this.nutritionService.calculateNutrition(weightInGrams, originProduct.nutrition);

    //     return {
    //         weightInGrams,
    //         totalNutritionValue,
    //         };
    // }

    async createNewIngredient(ingredientObj: IngrCreateDTO, recipe: Recipe): Promise<Ingredient> {
        const ingredient = new Ingredient();
        const originProduct = await this.productService.findProductByCode(ingredientObj.code);
        ingredient.product = originProduct;
        ingredient.quantity = ingredientObj.quantity;
        ingredient.unit = ingredientObj.unit;

        ingredient.currentRecipe = Promise.resolve(recipe);

        return await this.ingredientRepo.save(ingredient);
    }

    async calculateHelper(ingredient: Ingredient) {

        const originProduct = ingredient.product;
        const measureOfIngredient = originProduct.measures.find((measure) => {
            return `${measure.amount} ${measure.measure}` === ingredient.unit;
        });
        const weightInGrams = measureOfIngredient.gramsPerMeasure * ingredient.quantity;

        const totalNutritionValue = await this.nutritionService.calculateNutrition(weightInGrams, originProduct.nutrition);

        return {
            weightInGrams,
            totalNutritionValue,
        };
    }

    async getIngredientById( ingredientId: string ): Promise<Ingredient> {
        return await this.ingredientRepo.findOne({ where: { id: ingredientId }});
    }
    async deleteIngredientById(ingredientId: string) {
        const ingredientToDelete = await this.getIngredientById(ingredientId);
        const updatedField = { isDeleted: true };
        if (ingredientToDelete.isDeleted === false) {
            return await this.ingredientRepo.update(ingredientToDelete.id, updatedField);
        }
    }
    async updateIngredientById(ingredientToEdit: IngrUpdateDTO, recipe: Recipe): Promise<Ingredient> {
        const ingredient = await this.getIngredientById(ingredientToEdit.id);
        if (!ingredient) {
            throw new NotFoundException('Such ingredient cannot be found.');
        }
        if (ingredient.isDeleted === true) {
            throw new BadRequestException('This ingredient does not exist!');
        }
        if (ingredientToEdit.unit) {
            ingredient.unit = ingredientToEdit.unit;
        }
        if (ingredientToEdit.quantity) {
            ingredient.quantity = ingredientToEdit.quantity;
        }
        if (ingredientToEdit.isDeleted) {
            ingredient.isDeleted = ingredientToEdit.isDeleted;
        }
        return await this.ingredientRepo.save(ingredient);
    }
}
