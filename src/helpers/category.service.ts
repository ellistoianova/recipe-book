import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Category } from '../data/entities/category.entity';

@Injectable()
export class CategoryService {
    constructor(@InjectRepository(Category) private readonly categoryRepo: Repository<Category>,
    ) { }

    async getCategoryByName(catName: string): Promise<Category> {
        const category = await this.categoryRepo.findOne({ where: { name: catName } });
        if (!category) {
            throw new NotFoundException();
        }
        return category;
    }
}
