import { Injectable, NotFoundException } from '@nestjs/common';
import { Nutrition } from '../data/entities/nutrition.entity';
import { INutrition } from '../common/interfaces/nutrition';
import { Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class NutritionService {

    constructor(
        @InjectRepository(Nutrition) private readonly nutritionRepo: Repository < Nutrition >) {}

    calculateNutrition(weightInGrams: number, nutrition: Nutrition): INutrition {
        const calculateNutrient = (wghtInGr, field) => {
            return wghtInGr * field.value / 100;
        };
        const nutrients = {
            PROCNT: nutrition.PROCNT,
            FAT: nutrition.FAT,
            CHOCDF: nutrition.CHOCDF,
            ENERC_KCAL: nutrition.ENERC_KCAL,
            SUGAR: nutrition.SUGAR,
            FIBTG: nutrition.FIBTG,
            CA: nutrition.CA,
            FE: nutrition.FE,
            P: nutrition.P,
            K: nutrition.K,
            NA: nutrition.NA,
            VITA_IU: nutrition.VITA_IU,
            TOCPHA: nutrition.TOCPHA,
            VITD: nutrition.VITD,
            VITC: nutrition.VITC,
            VITB12: nutrition.VITB12,
            FOLAC: nutrition.FOLAC,
            CHOLE: nutrition.CHOLE,
            FATRN: nutrition.FATRN,
            FASAT: nutrition.FASAT,
            FAMS: nutrition.FAMS,
            FAPU: nutrition.FAPU,
        };
        for (const nutrient in nutrients) {
            if (nutrients.hasOwnProperty(nutrient)) {
                nutrients[nutrient].value = calculateNutrient(weightInGrams, nutrients[nutrient]);
            }
        }
        return nutrients;
    }

    calculateRecNutrPer100Grams(weightInGrams: number, nutrients: INutrition): INutrition {
        const calculateNutrPer100Grams = (wghtInGr, field) => {
            const calculatedField = +((100 * field.value / wghtInGr).toFixed(3));
            return calculatedField;
        };

        for (const nutrient in nutrients) {
            if (nutrients.hasOwnProperty(nutrient)) {
                nutrients[nutrient].value = calculateNutrPer100Grams(weightInGrams, nutrients[nutrient]);
            }
        }
        return nutrients;
    }

    sumNutritionValues(nutritionArray: INutrition[]): INutrition {
        return nutritionArray.reduce((acc, curr) => {
            for (const key in acc) {
                if (acc.hasOwnProperty(key)) {
                    acc[key].value += curr[key].value;
                }
            }
            return acc;
        });
    }
    async getNutritionById(nutritionId: string): Promise<Nutrition> {
        const nutrition = await this.nutritionRepo.findOne({ where: { id: nutritionId } });
        if (!nutrition) {
            throw new NotFoundException();
        }
        return nutrition;
    }
    async deleteNutritionById(nutritionId: string): Promise<UpdateResult> {
        const nutritionToDelete = await this.getNutritionById(nutritionId);
        if (!nutritionToDelete) {
            throw new NotFoundException();
        }
        const updatedField = { isDeleted: true };
        return await this.nutritionRepo.update(nutritionToDelete.id, updatedField);
    }
    async updateNutritionById(nutritionToUpdate, nutrientValues: INutrition): Promise<Nutrition> {
            nutritionToUpdate.PROCNT = nutrientValues.PROCNT;
            nutritionToUpdate.FAT = nutrientValues.FAT;
            nutritionToUpdate.CHOCDF = nutrientValues.CHOCDF;
            nutritionToUpdate.ENERC_KCAL = nutrientValues.ENERC_KCAL;
            nutritionToUpdate.SUGAR = nutrientValues.SUGAR;
            nutritionToUpdate.FIBTG = nutrientValues.FIBTG;
            nutritionToUpdate.CA = nutrientValues.CA;
            nutritionToUpdate.FE = nutrientValues.FE;
            nutritionToUpdate.P = nutrientValues.P;
            nutritionToUpdate.K = nutrientValues.K;
            nutritionToUpdate.NA = nutrientValues.NA;
            nutritionToUpdate.VITA_IU = nutrientValues.VITA_IU;
            nutritionToUpdate.TOCPHA = nutrientValues.TOCPHA;
            nutritionToUpdate.VITD = nutrientValues.VITD;
            nutritionToUpdate.VITC = nutrientValues.VITC;
            nutritionToUpdate.VITB12 = nutrientValues.VITB12;
            nutritionToUpdate.FOLAC = nutrientValues.FOLAC;
            nutritionToUpdate.CHOLE = nutrientValues.CHOLE;
            nutritionToUpdate.FATRN = nutrientValues.FATRN;
            nutritionToUpdate.FASAT = nutrientValues.FASAT;
            nutritionToUpdate.FAMS = nutrientValues.FAMS;
            nutritionToUpdate.FAPU = nutrientValues.FAPU;

            return await this.nutritionRepo.save(nutritionToUpdate);
    }
}
