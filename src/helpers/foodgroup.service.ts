import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ingredient } from '../data/entities/ingredient.entity';
import { Repository } from 'typeorm';
import { Subrecipe } from '../data/entities/subrecipe.entity';
import { FoodGroup } from '../data/entities/food-group.entity';

@Injectable()
export class FoodGroupService {
    constructor(@InjectRepository(FoodGroup) private readonly foodGroupRepo: Repository<FoodGroup>) { }

    async getFoodGroupsByCode( codes: number[]): Promise<FoodGroup[]> {
        const codesToFind = codes.map((code) => {
            return { code };
        });
        return await this.foodGroupRepo.find({ where: codesToFind });
    }
}
