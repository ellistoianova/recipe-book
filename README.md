## RECIPE BOOK API

Web Applications Teamwork Assignment for Telerik Academy Alpha and Fourth Bulgaria with JavaScript - Design and implement single-page web application using front-end framework of your choice.

The application will allow restaurant chefs to create and manage recipes composed of products with known nutrition values. During the interaction with the system users should see changes in nutrition information in real time. Newly created recipes could be tagged with a category from a list.
### Project Features

- x Authenticate users - Register, Login, Logout
- x Users can CRUD recipes
- x Recipes can contain as ingredients products from a list as well as other recipes.

### Stack

- Database - MariaDB
- REST API - NestJS

----------

# Getting started

## Installation

Clone the repository

    https://gitlab.com/ellistoianova/recipe-book.git

Switch to the repo folder

    cd recipe-book
    
Install dependencies
    
    npm install

----------

## Database

The example codebase uses [Typeorm](http://typeorm.io/) with a MariaDB database.

Create a new MariaDB database with the name `recipe_db` (or the name you specified in the ormconfig.json)

MariaDb database settings are in ormconfig.json

Start local MariaDB server and create new database 'recipe_db'


Run database migration

    npm run typeorm -- migration:run

Run the initial seed for the database
----------
    npm run seed
    



## NPM scripts

- `npm start` - Start application
- `npm run start:dev` - Start application in nodemon
- `npm run typeorm` - run Typeorm commands
- `npm run seed` - runs the inital seed for the database
- `npm run test` - run Jest test runner 


----------
